# markdown

## GNU/Linux Debian Buster con Vagrant

## Instalación de Virtualbox

Primero hay que descargar virtualbox, para esto nos vamos https://www.virtualbox.org y le damos en descargar y le damos en windows hosts para iniciar las desacarga de virtualbox para windows. Una vez descargado virtualbox procederemos con la instalación de este, aqui se aceptan los terminos de uso y se siguen las instrucciones de instalación del programa.

![virtualbox](/uploads/b0b90b81f33f7c5326d809299ff46166/virtualbox.PNG)
![virtualbox_install](/uploads/18951ea3dda115aa873db78af0bf05a3/virtualbox_install.PNG)

## Intalación de vagrant
Para la descarga de vagrant nos vamos a https://www.vagrantup.com/ y descargaremos la version de windows, una vez  descargado hacemos la instalación de este. Luego de una completa instalación del Vagrant vamos a cajas y buscamos bullseye y ingresamos en la mas reciente una vez dentro abrimos la terminal e ingresamos el comando vagrant add box debian/bullseye64 luego de estos el comando vagrant init debian/bullseye64 , una vez introducido dicho comando editamos el archivo vagrantfile con el comando code vagrant file una vez dentro desmarcamos la linea donde esta la red privada y por ultimo el comando vagrant up una vez finalizado, luego de esto iniciamos la maquina vagrant hay múltiples formas para hacerlo, pero en este caso yo use el comando vagrant ssh que fue el que me funciono de manera correcta.
![vagrant](/uploads/07d842c25cd938425d65519da0f61533/vagrant.PNG)
![vagrant_install](/uploads/8cf43c17a08eae354d3a0acca050b22d/vagrant_install.PNG)
![Captura_1](/uploads/d5bab942bc24c9510970ced84cad65ab/Captura_1.PNG)
![Captura_2](/uploads/3ca425fef8352e91c7a302c0e2642c7a/Captura_2.PNG)
![Captura_3](/uploads/8da8e0b5c1236c0c0589ae1274e94ca6/Captura_3.PNG)
![Captura_4](/uploads/53da62de1e5868952652db4dd43e5d70/Captura_4.PNG)
![Captura_5](/uploads/018e778e28f16ce20a48925b86dd7e53/Captura_5.PNG)
![Captura_6](/uploads/4faa93bafcbaea63eb1b29af0897a681/Captura_6.PNG)
![Captura_7](/uploads/834307fc10803f28b1d68cbd3c2e24a6/Captura_7.PNG)
![Captura_8](/uploads/67f7b1b170059cba28ea0578b9c9c89e/Captura_8.PNG)



## Instalación de los programas para servidor web (Apache2, PHP, MySQL y librerías)
Una vez iniciada la maquina procederemos a crear un nuevo usuario con el comando sudo add user laravel, luego del usuario laravel ser creado agregamos al usuario laravel al grupo sudo con el comando sudo gpasswd -a laravel sudo, una vez agregado instalamos apache 2 , php , mysql y otras librerias pero debemos escribir el comando sudo apt-get update para actualizar las librerias antes de seguir, luego de actualizadas ejecutamos el comando sudo apt-get install apache2 php7.4 mariadb-server mariadb-client 
![Captura_9](/uploads/e7014e7b111d44fff291c96ebaa0accd/Captura_9.PNG)
![Captura_10](/uploads/3e7478188067f5839c3e818ef037944f/Captura_10.PNG)
![Captura_11](/uploads/b9a9fbd0e7fd7a3f0667dd37c6d35c50/Captura_11.PNG)
![Captura_12](/uploads/e5e1913e039542eee0a8a4b96179a0bb/Captura_12.PNG)




## Prueba de sitio del servidor
Una vez instalado apache nos vamos a nuestra maquina servidor a configurar en los host la dirección del web server y agregamos la ip 192.168.33.10 con el nombre webserver y luego nos vamos al navegador para ingresar ya sea la IP o el dominio, con esto vamos a lograr verificar que nuestro servidor esta funcionando correctamente.
![Captura_13](/uploads/cec34aeb741c5e6bfa82cdab209ad0ad/Captura_13.PNG)




## Creación de dos sitios con Laravel
Para la creación de sitios laravel primero tenemos que recurrir a la instalación de composer para eso ingresamos a la pagina y buscamos la instalación, luego de tener instalado composer daremos inicio con la creacion de los archivos laravel:

-composer global require laravel/installer

-laravel new laravel.isw811.xyz


Y hacemos lo mismo con el otro archivo:

-composer global require laravel/installer

-laravel new blog.isw811.xyz

![Captura_14](/uploads/870128c759cd11f680cde6aa4d9ac29e/Captura_14.PNG)
![Captura_15](/uploads/8bcf979ba3a5d1c5bc9adf3960a30a27/Captura_15.PNG)
![Captura_16](/uploads/bbbcfb26a6f7ce98183da31cbd6248a2/Captura_16.PNG)




## Configuración para hospedar varios sitios

En la maquina creamos dos archivos con el comando touch laravel.isw811.xyz.conf blog.isw811.xyz.conf
luego de creados los editamos con la información correspondiente para un vhost, luego de editados los movemos con el comando sudo mv *.conf /rtc/apache2/sites-avaible/  luego los activamos con los módulos del comando sudo a2enmod vhost_alias rewrite ssl y luego reiniciamos apache con systemctl restart apache

![Captura_17](/uploads/9cb96ba8e6ddfaea25636058b540be61/Captura_17.PNG)



## Configuración de archivos hosts para simular la resolución del dominio

Luego configuramos los host de nuestra maquina servidor con la IP y los nombres de los sitios 

![Captura_18](/uploads/14c35e4804966cd3ca0bfbf38880f649/Captura_18.PNG)
![Captura_19](/uploads/a17209533abe9ce0907a7d0e14bd5760/Captura_19.PNG)




## Creación de base de datos para Laravel

Con el comando sudo mysql entramos a mariaDB una vez dentro creamos la base de datos y luego un usuario y le asignamos sus roles una vez finalizado nos vamos a los sitios de laravel  a configurar en el archivo .env los datos. Una vez finalizado ya tendriamos nuestros 2 sitios completamente listos.
![Captura_20](/uploads/73a2a00eff9e69b671dadc83137b8c61/Captura_20.PNG)

